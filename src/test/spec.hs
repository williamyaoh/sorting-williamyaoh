module Main where

import Control.Monad.IO.Class ( liftIO )
import Data.Array ( Array )
import Data.Vector ( Vector )
import Hedgehog ( PropertyT, forAll, assert, annotate )
import Test.Hspec ( hspec, parallel, describe, it )
import Test.Hspec.Hedgehog ( modifyMaxSuccess, modifyMaxSize, hedgehog )

import qualified Data.Array as Arr
import qualified Data.Array.MArray as Arr
import qualified Data.Sorting as Sort
import qualified Data.Sorting.Array as SortArr
import qualified Data.Sorting.Vector as SortVec
import qualified Data.Vector as Vec
import qualified Data.Vector.Mutable as Vec
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

main :: IO ()
main = hspec $ parallel $ modifyMaxSuccess (const 1000) $ modifyMaxSize (const 1000) $ do
  describe "Data.Sorting.Vector" $ do
    it "sorts Int64 Vector correctly" $
      hedgehog sortsVecI64
    it "sorts Text Vector correctly" $
      hedgehog sortsVecText
  describe "Data.Sorting.Array" $ do
    it "sorts Int64 Array correctly" $
      hedgehog sortsArrI64
    it "sorts Text Array correctly" $
      hedgehog sortsArrText

sortsVecI64 :: PropertyT IO ()
sortsVecI64 = do
  elems <- forAll $ Gen.list (Range.exponential 0 1000) (Gen.int64 $ Range.linear 0 10000)
  let immut = Vec.fromList elems
  arr <- liftIO $ Vec.thaw immut
  liftIO $ SortVec.sortI64 arr
  sorted <- Vec.freeze arr
  annotate "sortI64 did not return a sorted vector"
  assert $ vecIsSorted sorted

sortsVecText :: PropertyT IO ()
sortsVecText = do
  elems <- forAll $ Gen.list (Range.exponential 0 1000) (Gen.text (Range.linear 0 100) Gen.unicode)
  let immut = Vec.fromList elems
  arr <- liftIO $ Vec.thaw immut
  liftIO $ SortVec.sortText arr
  sorted <- Vec.freeze arr
  annotate "sortText did not return a sorted vector"
  assert $ vecIsSorted sorted

sortsArrI64 :: PropertyT IO ()
sortsArrI64 = do
  elems <- forAll $ Gen.list (Range.exponential 0 1000) (Gen.int64 $ Range.linear 0 10000)
  arr <- liftIO $ Arr.newListArray (0, length elems - 1) elems
  liftIO $ SortArr.sortI64 arr
  sorted <- liftIO $ Arr.freeze arr
  annotate "sortI64 did not return a sorted array"
  assert $ arrIsSorted sorted

sortsArrText :: PropertyT IO ()
sortsArrText = do
  elems <- forAll $ Gen.list (Range.exponential 0 1000) (Gen.text (Range.linear 0 100) Gen.unicode)
  arr <- liftIO $ Arr.newListArray (0, length elems - 1) elems
  liftIO $ SortArr.sortText arr
  sorted <- liftIO $ Arr.freeze arr
  annotate "sortText did not return a sorted array"
  assert $ arrIsSorted sorted

vecIsSorted :: Ord a => Vector a -> Bool
vecIsSorted vec =
  flip all (scanr (:) [] (Vec.toList vec)) $ \head -> case head of
    (x:y:_) -> x <= y
    _ -> True

arrIsSorted :: Ord a => Array Int a -> Bool
arrIsSorted arr =
  flip all (scanr (:) [] (Arr.elems arr)) $ \head -> case head of
    (x:y:_) -> x <= y
    _ -> True
