{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.Sorting where

import Control.DeepSeq ( NFData(..) )
import Criterion
import Criterion.Main ( bench, bgroup, perRunEnv )
import Data.Array.IO ( IOArray )
import Data.Char ( chr )
import Data.Foldable ( for_, toList )
import Data.Int ( Int64 )
import Data.Sequence ( Seq )
import Data.Text ( Text )
import Data.Vector ( Vector )
import Data.Vector.Mutable ( PrimMonad(..) )
import System.IO.Unsafe ( unsafePerformIO )
import System.Random ( randomIO )
import Test.QuickCheck
import Test.QuickCheck.Unicode

import qualified Data.Array.MArray as Arr
import qualified Data.List as List
import qualified Data.Sequence as Seq
import qualified Data.Sorting.Array as SortArr
import qualified Data.Sorting.Vector as SortVec
import qualified Data.Text as Text
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as Vec
import qualified Data.Vector.Algorithms.Heap as Heap
import qualified Data.Vector.Algorithms.Intro as Intro
import qualified Data.Vector.Algorithms.Merge as Merge

i64Bench :: Int -> [Benchmark]
i64Bench n =
  [ bgroup "i64"
    [ bench "List" $ perRunEnv (randomListI64 n) $ \elems ->
        pure $! List.sort elems
    , bench "Seq" $ perRunEnv (randomSeqI64 n) $ \elems ->
        pure $! Seq.unstableSort elems
    , bench "VA heapsort" $ perRunEnv (randomMVectorI64 n) $ \elems ->
        Heap.sort elems
    , bench "VA mergesort" $ perRunEnv (randomMVectorI64 n) $ \elems ->
        Merge.sort elems
    , bench "VA introsort" $ perRunEnv (randomMVectorI64 n) $ \elems ->
        Intro.sort elems
    ]
  ]

copyBench :: Int -> [Benchmark]
copyBench n =
  [ bgroup "copy"
    [ bench "List" $ perRunEnv (randomListI64 n) $ \elems ->
        pure $! List.sort elems
    , bench "Seq" $ perRunEnv (randomSeqI64 n) $ \elems ->
        pure $! Seq.sort elems
    , bgroup "VA introsort"
      [ bench "from List" $ perRunEnv (randomListI64 n) $ \elems -> do
          let immut = V.fromList elems
          mut <- V.thaw immut
          Intro.sort mut
          sorted <- V.freeze mut
          pure $ V.toList sorted
      , bench "from Seq" $ perRunEnv (randomSeqI64 n) $ \elems -> do
          let immut = V.fromList $ toList elems
          mut <- V.thaw immut
          Intro.sort mut
          sorted <- V.freeze mut
          pure $ Seq.fromList $ V.toList sorted
      , bench "from Vector" $ perRunEnv (randomVectorI64 n) $ \elems -> do
          mut <- V.thaw elems
          Intro.sort mut
          V.freeze mut :: IO (Vector Int64)
      ]
    ]
  ]

textBench :: Int -> [Benchmark]
textBench n =
  [ bgroup "text"
    [ bench "List" $ perRunEnv (randomListText n) $ \elems ->
        pure $! List.sort elems
    , bench "Seq" $ perRunEnv (randomSeqText n) $ \elems ->
        pure $! Seq.unstableSort elems
    , bench "VA heapsort" $ perRunEnv (randomMVectorText n) $ \elems ->
        Heap.sort elems
    , bench "VA mergesort" $ perRunEnv (randomMVectorText n) $ \elems ->
        Merge.sort elems
    , bench "VA introsort" $ perRunEnv (randomMVectorText n) $ \elems ->
        Intro.sort elems
    ]
  ]

handwrittenBench :: Int -> [Benchmark]
handwrittenBench n =
  [ bgroup "handwritten"
    [ bench "VA introsort" $ perRunEnv (randomMVectorI64 n) $ \elems ->
        Intro.sort elems
    , bench "monomorphic Array quicksort" $ perRunEnv (randomMArrayI64 n) $ \elems ->
        SortArr.sortI64 elems
    , bench "monomorphic Vector quicksort" $ perRunEnv (randomMVectorI64 n) $ \elems ->
        SortVec.sortI64 elems
    , bench "polymorphic Vector quicksort" $ perRunEnv (randomMVectorI64 n) $ \elems ->
        SortVec.sort elems
    ]
  ]

randomListI64 :: Int -> IO [Int64]
randomListI64 size =
  traverse (\_ -> randomIO) [1..size]

randomListText :: Int -> IO [Text]
randomListText size =
  traverse (\_ -> generate text) [1..size]

randomSeqI64 :: Int -> IO (Seq Int64)
randomSeqI64 size = do
  elems <- traverse (\_ -> randomIO) [1..size]
  pure $ Seq.fromList elems

randomSeqText :: Int -> IO (Seq Text)
randomSeqText size = do
  elems <- traverse (\_ -> generate text) [1..size]
  pure $ Seq.fromList elems

randomVectorI64 :: Int -> IO (Vector Int64)
randomVectorI64 size =
  V.generateM size (\_ -> randomIO)

randomVectorText :: Int -> IO (Vector Text)
randomVectorText size =
  V.generateM size (\_ -> generate text)

randomMVectorI64 :: Int -> IO (Vec.IOVector Int64)
randomMVectorI64 size =
  Vec.generateM size (\_ -> randomIO)

randomMVectorText :: Int -> IO (Vec.IOVector Text)
randomMVectorText size =
  Vec.generateM size (\_ -> generate text)

randomMArrayI64 :: Int -> IO (IOArray Int Int64)
randomMArrayI64 size = do
  elems <- traverse (\_ -> randomIO) [1..size]
  Arr.newListArray (0, length elems - 1) elems

randomMArrayText :: Int -> IO (IOArray Int Text)
randomMArrayText size = do
  elems <- traverse (\_ -> generate text) [1..size]
  Arr.newListArray (0, length elems - 1) elems

instance NFData a => NFData (Vec.IOVector a) where
  rnf arr = unsafePerformIO $ go arr 0 (Vec.length arr)
    where
      go _arr ix len | ix >= len = pure ()
      go arr ix len = do
        unit <- go arr (ix+1) len
        here <- Vec.unsafeRead arr ix
        pure $! rnf here `seq` unit

instance NFData a => NFData (IOArray Int a) where
  rnf arr = unsafePerformIO $ do
    (lo, hi) <- Arr.getBounds arr
    go arr lo hi
    where
      go _arr ix hi | ix >= hi = pure ()
      go arr ix hi = do
        unit <- go arr (ix+1) hi
        here <- Arr.readArray arr ix
        pure $! rnf here `seq` unit

text :: Gen Text
text = Text.pack <$> listOf (pure 'A')

unicodeChar :: Gen Char
unicodeChar = chr `fmap` points
  where points = flip suchThat (not . reserved) $ oneof
          [ ascii
          , plane0
          , plane1
          , plane2
          , plane14
          ]
