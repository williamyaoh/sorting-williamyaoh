{-# LANGUAGE ScopedTypeVariables #-}

module Data.Sorting.Vector
  ( sortI64, sortText, sort )
where

import Control.Monad ( when )
import Data.Int ( Int64 )
import Data.Text ( Text )
import Data.Vector.Mutable ( IOVector )

import qualified Data.Vector.Mutable as Vec

sortI64 :: IOVector Int64 -> IO ()
sortI64 arr =
  go 0 (Vec.length arr - 1)
  where
    go :: Int -> Int -> IO ()
    go lo hi = when (lo < hi) $ do
      mid <- partitionI64 lo hi arr
      go lo (mid-1)
      go (mid+1) hi

partitionI64 :: Int -> Int -> IOVector Int64 -> IO Int
partitionI64 lo hi arr =
  Vec.read arr hi >>= go lo lo
  where
    go :: Int -> Int -> Int64 -> IO Int
    go i j piv =
      if j < hi then do
        next <- Vec.read arr j
        if next <= piv then do
          Vec.swap arr i j
          go (i+1) (j+1) piv
        else go i (j+1) piv
      else do
        Vec.swap arr i hi
        pure i

sortText :: IOVector Text -> IO ()
sortText arr =
  go 0 (Vec.length arr - 1)
  where
    go :: Int -> Int -> IO ()
    go lo hi = when (lo < hi) $ do
      mid <- partitionText lo hi arr
      go lo (mid-1)
      go (mid+1) hi

partitionText :: Int -> Int -> IOVector Text -> IO Int
partitionText lo hi arr = do
  Vec.read arr hi >>= go lo lo
  where
    go :: Int -> Int -> Text -> IO Int
    go i j piv =
      if j < hi then do
        next <- Vec.read arr j
        if next <= piv then do
          Vec.swap arr i j
          go (i+1) (j+1) piv
        else go i (j+1) piv
      else do
        Vec.swap arr i hi
        pure i

sort :: Ord a => IOVector a -> IO ()
sort arr =
  go 0 (Vec.length arr - 1)
  where
    go :: Int -> Int -> IO ()
    go lo hi = when (lo < hi) $ do
      mid <- partition lo hi arr
      go lo (mid-1)
      go (mid+1) hi

partition :: forall a. Ord a => Int -> Int -> IOVector a -> IO Int
partition lo hi arr =
  Vec.read arr hi >>= go lo lo
  where
    go :: Int -> Int -> a -> IO Int
    go i j piv =
      if j < hi then do
        next <- Vec.read arr j
        if next <= piv then do
          Vec.swap arr i j
          go (i+1) (j+1) piv
        else go i (j+1) piv
      else do
        Vec.swap arr i hi
        pure i
