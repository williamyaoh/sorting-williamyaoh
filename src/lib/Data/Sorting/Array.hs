module Data.Sorting.Array
  ( sortI64, sortText )
where

import Control.Monad ( when )
import Data.Int ( Int64 )
import Data.Text ( Text )
import Data.Array.IO ( IOArray )

import qualified Data.Array.MArray as Arr

sortI64 :: IOArray Int Int64 -> IO ()
sortI64 arr = do
  (lo, hi) <- Arr.getBounds arr
  go lo hi
  where
    go :: Int -> Int -> IO ()
    go lo hi = when (lo < hi) $ do
      mid <- partitionI64 lo hi arr
      go lo (mid-1)
      go (mid+1) hi

partitionI64 :: Int -> Int -> IOArray Int Int64 -> IO Int
partitionI64 lo hi arr =
  Arr.readArray arr hi >>= go lo lo
  where
    go :: Int -> Int -> Int64 -> IO Int
    go i j piv =
      if j < hi then do
        next <- Arr.readArray arr j
        if next <= piv then do
          prev <- Arr.readArray arr i
          Arr.writeArray arr i next
          Arr.writeArray arr j prev
          go (i+1) (j+1) piv
        else go i (j+1) piv
      else do
        here <- Arr.readArray arr i
        Arr.writeArray arr i piv
        Arr.writeArray arr hi here
        pure i

sortText :: IOArray Int Text -> IO ()
sortText arr = do
  (lo, hi) <- Arr.getBounds arr
  go lo hi
  where
    go :: Int -> Int -> IO ()
    go lo hi = when (lo < hi) $ do
      mid <- partitionText lo hi arr
      go lo (mid-1)
      go (mid+1) hi

partitionText :: Int -> Int -> IOArray Int Text -> IO Int
partitionText lo hi arr =
  Arr.readArray arr hi >>= go lo lo
  where
    go :: Int -> Int -> Text -> IO Int
    go i j piv =
      if j < hi then do
        next <- Arr.readArray arr j
        if next <= piv then do
          prev <- Arr.readArray arr i
          Arr.writeArray arr i next
          Arr.writeArray arr j prev
          go (i+1) (j+1) piv
        else go i (j+1) piv
      else do
        here <- Arr.readArray arr i
        Arr.writeArray arr i piv
        Arr.writeArray arr hi here
        pure i
