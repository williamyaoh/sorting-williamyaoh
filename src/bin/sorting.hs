module Main where

import Criterion.Main ( defaultMainWith, defaultConfig )
import Criterion.Types ( Config(..) )
import System.Environment ( getArgs )

import qualified Data.Sorting as Sorting

main :: IO ()
main = do
  [suite, n_] <- getArgs
  let n = read n_
  let cfg = config suite n_
  case suite of
    "i64" -> defaultMainWith cfg (Sorting.i64Bench n)
    "text" -> defaultMainWith cfg (Sorting.textBench n)
    "copy" -> defaultMainWith cfg (Sorting.copyBench n)
    "handwritten" -> defaultMainWith cfg (Sorting.handwrittenBench n)
    _ -> fail "unknown benchmark suite. supported values are: i64, text, copy, handwritten"
  where
    config :: String -> String -> Config
    config suite n_ = defaultConfig
      { reportFile = Just (suite ++ "-" ++ n_ ++ ".html")
      , csvFile = Just (suite ++ "-" ++ n_ ++ ".csv")
      }
